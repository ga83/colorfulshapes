# Colorful Shapes Extreme Android Wallpaper.  
Uses LibGDX game development framework.
Licenced under BSD.

[https://play.google.com/store/apps/details?id=com.beachcafesoftware.colorfulshapes](https://play.google.com/store/apps/details?id=com.beachcafesoftware.colorfulshapes)