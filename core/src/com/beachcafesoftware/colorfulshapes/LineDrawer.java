package com.beachcafesoftware.colorfulshapes;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Random;

/**
 * Guy Aaltonen
 **/

public class LineDrawer {
    static Random rnd;
    static float[] points;

    public LineDrawer() {
        points = new float[20];
        rnd = new Random();
    }

    public static void drawLine(SpriteBatch sb, float x1, float y1, float x2, float y2, short[] color, int width, int height, Texture t1, boolean shake,
    float lineWidth, float opacity) {
        float colori = com.badlogic.gdx.graphics.Color.toFloatBits(
                color[0] / 255f,
                color[1] / 255f,
                color[2] / 255f,
                opacity
        );

        if (shake == true) {
            x1 += -0.005 + rnd.nextFloat() * 0.01;
            x2 += -0.005 + rnd.nextFloat() * 0.01;
            y1 += -0.005 + rnd.nextFloat() * 0.01;
            y2 += -0.005 + rnd.nextFloat() * 0.01;
        }

        x1 *= width;
        x2 *= width;
        y1 *= height;
        y2 *= height;

        float angle = (float) Math.toDegrees(Math.atan((y2 - y1) / (x2 - x1))) + 90;
        float xp = (float) (Math.cos(Math.toRadians(angle)) * lineWidth * 0.75);
        float yp = (float) (Math.sin(Math.toRadians(angle)) * lineWidth * 0.75);

        points[0] = x1 + xp;
        points[1] = y1 + yp;
        points[2] = colori;
        points[3] = 0;
        points[4] = 0;
        points[5] = x2 + xp;
        points[6] = y2 + yp;
        points[7] = colori;
        points[8] = 1;
        points[9] = 0;
        points[10] = x2 - xp;
        points[11] = y2 - yp;
        points[12] = colori;
        points[13] = 1;
        points[14] = 1;
        points[15] = x1 - xp;
        points[16] = y1 - yp;
        points[17] = colori;
        points[18] = 0;
        points[19] = 1;

        sb.draw(t1, points, 0, 20);

    }

    public static void darken(SpriteBatch sb, int width, int height, Texture t1) {
        float colori = com.badlogic.gdx.graphics.Color.toFloatBits(
            0, 0, 0, 0.02f
        );

        int x1; int x2; int y1; int y2;
        x1 = 0;
        x2 = width;
        y1 = 0;
        y2 = height;

        points[0] = x1 ;
        points[1] = y1 ;
        points[2] = colori;
        points[3] = 0;
        points[4] = 0;
        points[5] = x2 ;
        points[6] = y1 ;
        points[7] = colori;
        points[8] = 1;
        points[9] = 0;
        points[10] = x2 ;
        points[11] = y2 ;
        points[12] = colori;
        points[13] = 1;
        points[14] = 1;
        points[15] = x1 ;
        points[16] = y2 ;
        points[17] = colori;
        points[18] = 0;
        points[19] = 1;

        sb.draw(t1, points, 0, 20);
    }
}
