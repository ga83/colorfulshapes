package com.beachcafesoftware.colorfulshapes;

import java.util.Random;


import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteCache;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.TimeUtils;

/*
** Guy Aaltonen
*/

public class ColorfulShapesExtreme extends ApplicationAdapter {

    public static boolean prefsChanged;
    private SpriteBatch sb;
    private SpriteCache sc;
    private Texture t1;
    private ShaderProgram shader;
    private int currentFrame = 0;
    private Random rnd;
    private Preferences prefs;
    private long runningTime;
    private boolean shake;
    private long lastSwitch;
    private PolygonAndroid[] polygons = new PolygonAndroid[20];
    private DrawcallContainer dcc = new DrawcallContainer();
    private int numPolygons;
    private final int trailLength = 3100;
    private long lastFrame = TimeUtils.millis();
    private FrameBuffer fb;
    private int maxShapes;
    private float lineWidth;
    private float opacity;
    private boolean blackout;


    @Override
    public void create() {
        new LineDrawer();
        rnd = new Random();
        sb = new SpriteBatch();
        sc = new SpriteCache();
        randomise();
    }

    private void sanitisePreferences() {
        String runTimeS = prefs.getString("runningtime");
        String shakeS = prefs.getString("shake");
        String maxShapesS = prefs.getString("maxshapes");
        String lineWidthS = prefs.getString("linewidth");
        String opacityS = prefs.getString("opacity");
        String blackoutS = prefs.getString("blackout");

        // defaults if empty
        if (runTimeS.equals("")) {
            prefs.putString("runningtime", "30");
        }
        if (shakeS.equals("")) {
            prefs.putString("shake", "false");
        }
        if (maxShapesS.equals("")) {
            prefs.putString("maxshapes", "4");
        }
        if (lineWidthS.equals("")) {
            prefs.putString("linewidth", "1.0");
        }
        if (opacityS.equals("")) {
            prefs.putString("opacity", "0.75");
        }
        if (blackoutS.equals("")) {
            prefs.putString("blackout", "true");
        }

        prefs.flush();
    }

    public void randomise() {
        prefs = Gdx.app.getPreferences("cube2settings");
        sanitisePreferences();

        this.runningTime = Integer.parseInt(prefs.getString("runningtime"));
        this.shake = Boolean.parseBoolean(prefs.getString("shake"));
        this.maxShapes = Integer.parseInt(prefs.getString("maxshapes"));
        this.lineWidth = Float.parseFloat(prefs.getString("linewidth"));
        this.opacity = Float.parseFloat(prefs.getString("opacity"));
        this.blackout = Boolean.parseBoolean(prefs.getString("blackout"));

        this.lastSwitch = TimeUtils.millis();
        this.numPolygons = 1 + rnd.nextInt(maxShapes);
        boolean useTheme = rnd.nextBoolean();
        int seed = rnd.nextInt();

        for (int i = 0; i < numPolygons; i++) {
            polygons[i] = new PolygonAndroid(
                    3 + rnd.nextInt(5),
                    0,
                    -5.0 + rnd.nextDouble() * 10.0,
                    rnd.nextFloat(),
                    rnd.nextFloat(),
                    1.15,
                    useTheme,
                    seed,
                    -1
            );
        }

        Pixmap p1 = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        p1.setColor(1, 1, 1, 1);
        p1.fill();
        t1 = new Texture(p1);
        dcc.position = 0;

        if(fb == null) {
            int width = Gdx.graphics.getWidth();
            int height = Gdx.graphics.getHeight();
            fb = new FrameBuffer(Pixmap.Format.RGBA8888, width, height, false);
        }
    }


    @Override
    public void render() {
        if (ColorfulShapesExtreme.prefsChanged == true) {
            this.lastSwitch = 0;
            prefsChanged = false;
        }

        if (currentFrame % 20 == 0) {
            sb.dispose();
            sb = new SpriteBatch();
        }

        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();

        fb.begin();
        sb.begin();

        if(blackout == true) {
            if (TimeUtils.millis() < this.lastSwitch + 2000) {
                LineDrawer.darken(sb, width, height, t1);
            }
        }

        for (int i = 0; i < numPolygons; i++) {
            polygons[i].update();
            polygons[i].draw(sb, false, dcc, width, height, t1, shake, lineWidth, opacity);
        }


        sb.end();
        fb.end();

        sb.begin();
        sb.draw(fb.getColorBufferTexture(),0,0,width,height);
        sb.end();

        currentFrame++;

        if (TimeUtils.millis() - this.lastSwitch > this.runningTime * 1000) {
            randomise();
            currentFrame = 0;
        }

        long thisFrame = TimeUtils.millis();
        long interval = thisFrame - lastFrame;


        try {
            long timeToSleep = Math.max(1000 / 30 - interval, 0);
            Thread.sleep(timeToSleep);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        lastFrame = thisFrame;
    }
}
