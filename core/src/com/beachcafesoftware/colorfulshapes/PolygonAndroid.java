package com.beachcafesoftware.colorfulshapes;

import java.util.Random;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/*
* Guy Aaltonen
*/

public class PolygonAndroid {
    int n; // number of sides
    double dtheta; // rotation per update
    double theta; // current rotation
    double xc;
    double yc;
    double radius;
    short[][] edgecolours = new short[100][3];
    int halfResizePeriod;
    int resizeProgress;
    boolean growing;
    double[] pointsx;
    double[] pointsy;
    static short[][] hs = new short[6][3];


    void update() {
        theta += dtheta;
        if (theta >= 360) {
            theta -= 360;
        }
        if (theta < 0) {
            theta += 360;
        }

        resizeProgress++;
        if (growing == true) {
            radius = (resizeProgress / (double) halfResizePeriod) * 1.6;
        } else {
            radius = (1 - (resizeProgress / (double) halfResizePeriod)) * 1.6;
        }
        if (resizeProgress == halfResizePeriod) {
            resizeProgress = 0;
            growing = !growing;
        }
    }

    void draw(SpriteBatch sb, boolean record, DrawcallContainer dcc, int width, int height, Texture t1, boolean shake,
    float lineWidth, float opacity) {
        double segmenttheta;
        double y;
        double x;

        double shortside = Math.min(height, width);
        double longside = Math.max(height, width);

        int c = 0;
        while (c < n) {
            segmenttheta = ((360 / (double) n) * (c - 1) + this.theta);
            if (longside == width) {
                y = Math.sin(Math.toRadians(segmenttheta)) * radius;
                x = Math.cos(Math.toRadians(segmenttheta)) * radius * (shortside / longside);
            } else {
                y = Math.sin(Math.toRadians(segmenttheta)) * radius * (shortside / longside);
                x = Math.cos(Math.toRadians(segmenttheta)) * radius;
            }

            pointsx[c] = x;
            pointsy[c] = y;

            c++;
        }

        c = 0;

        while (c < n) {
            if (c < n - 1) {
                if (record == false) {
                    LineDrawer.drawLine(sb,
                            (float) (pointsx[c] + xc),
                            (float) (pointsy[c] + yc),
                            (float) (pointsx[c + 1] + xc),
                            (float) (pointsy[c + 1] + yc),
                            edgecolours[c], width, height, t1, shake, lineWidth, opacity);
                } else {
                    dcc.drawcalls[dcc.position].color = edgecolours[c];
                    dcc.drawcalls[dcc.position].coordinates[0] = (float) (pointsx[c] + xc);
                    dcc.drawcalls[dcc.position].coordinates[1] = (float) (pointsy[c] + yc);
                    dcc.drawcalls[dcc.position].coordinates[2] = (float) (pointsx[c + 1] + xc);
                    dcc.drawcalls[dcc.position].coordinates[3] = (float) (pointsy[c + 1] + yc);
                    dcc.position++;
                }
            } else {
                if (record == false) {
                    LineDrawer.drawLine(sb,
                            (float) (pointsx[c] + xc),
                            (float) (pointsy[c] + yc),
                            (float) (pointsx[0] + xc),
                            (float) (pointsy[0] + yc),
                            edgecolours[c], width, height, t1, shake, lineWidth, opacity);
                } else {
                    dcc.drawcalls[dcc.position].color = edgecolours[c];
                    dcc.drawcalls[dcc.position].coordinates[0] = (float) (pointsx[c] + xc);
                    dcc.drawcalls[dcc.position].coordinates[1] = (float) (pointsy[c] + yc);
                    dcc.drawcalls[dcc.position].coordinates[2] = (float) (pointsx[0] + xc);
                    dcc.drawcalls[dcc.position].coordinates[3] = (float) (pointsy[0] + yc);
                    dcc.position++;
                }
            }
            c++;

        }
    }

    static Random creatorRandom = new Random();

    public PolygonAndroid(int n, double theta, double dtheta, double xc,
                          double yc, double radius, boolean useScheme, int seed, int halfResizePeriod) {
        this.radius = radius;
        this.n = n;
        this.dtheta = dtheta;
        this.theta = theta;
        this.yc = yc;
        this.xc = xc;
        this.resizeProgress = 0;
        this.growing = creatorRandom.nextBoolean();
        initHighSat();
        randomiseColors(useScheme, seed);

        if (halfResizePeriod == -1) {
            this.halfResizePeriod = 150 + creatorRandom.nextInt(1000);
        } else {
            this.halfResizePeriod = halfResizePeriod;

        }
        this.resizeProgress = creatorRandom.nextInt(this.halfResizePeriod / 2);

        pointsx = new double[n];
        pointsy = new double[n];

    }


    public void randomiseColors(boolean useScheme, int seed) {
        Random rnd = new Random();

        if (useScheme == true) {
            randomiseScheme(seed);
            return;
        } else {
            for (int i = 0; i < edgecolours.length; i++) {
                short min = 64;

                short r = (short) (min + rnd.nextInt(255 - min));
                short g = (short) (min + rnd.nextInt(255 - min));
                short b = (short) (min + rnd.nextInt(255 - min));

                // increase saturation?
                if (rnd.nextDouble() < 0.5) {
                    this.edgecolours[i] = (short[]) getRandomHighSaturation(rnd);
                } else {
                    this.edgecolours[i] = new short[] { r, g, b };
                }
            }
        }

    }

    public void randomiseScheme(int seed) {
        Random rnd = new Random(seed);

        boolean useHighSaturation = rnd.nextBoolean();

        short[] col1 = getRandomColor(rnd);
        short[] col2;
        if (useHighSaturation == true) {
            col2 = getRandomHighSaturation(rnd);
        } else {
            col2 = getRandomColor(rnd);
        }
        double redInterval = (col2[0] - col1[0])
                / (double) (this.n - 1);
        double greenInterval = (col2[1] - col1[1])
                / (double) (this.n - 1);
        double blueInterval = (col2[2] - col1[2])
                / (double) (this.n - 1);

        for (int i = 0; i < this.n; i++) {
            if (useHighSaturation == true && i == 0) {
                edgecolours[i] = getRandomHighSaturation(rnd);

            } else {
                edgecolours[i] = new short[]{
                        (short) (col1[0] + redInterval * i),
                        (short) (col1[1] + greenInterval * i),
                        (short) (col1[2] + blueInterval * i)
                };
            }
        }
    }

    private void initHighSat() {
        hs[0] = new short[] { 255, 0, 255 };
        hs[1] = new short[] { 255, 0, 0 };
        hs[2] = new short[] { 255, 255, 0 };
        hs[3] = new short[] { 0, 0, 255 };
        hs[4] = new short[] { 0, 255, 255 };
        hs[5] = new short[] { 0, 255, 0 };
    }

    private short[] getRandomHighSaturation(Random rnd) {
        int index = rnd.nextInt(6);
        return hs[index];
    }

    private short[] getRandomColor(Random rnd) {
        int min = 64;
        short r;
        short g;
        short b;
        r = (short) (min + rnd.nextInt(255 - min));
        g = (short) (min + rnd.nextInt(255 - min));
        b = (short) (min + rnd.nextInt(255 - min));

        return new short[] { r, g, b };
    }
}

class DrawcallContainer {
    Drawcall[] drawcalls;
    int position;

    public void draw(int lastN, SpriteBatch sb, int width, int height, Texture t1, boolean shake,
    float lineWidth, float opacity) // draws the last n calls only
    {
        int c = position - lastN;

        if (c < 0) {
            c = 0;
        }
        for (int i = c; i < position; i++) {
            LineDrawer.drawLine(
                    sb,
                    drawcalls[i].coordinates[0],
                    drawcalls[i].coordinates[1],
                    drawcalls[i].coordinates[2],
                    drawcalls[i].coordinates[3],
                    drawcalls[i].color,
                    width,
                    height,
                    t1,
                    shake,
                    lineWidth,
                    opacity
            );
        }
    }

    public DrawcallContainer() {
        position = 0;
        drawcalls = new Drawcall[100000];

        for (int i = 0; i < drawcalls.length; i++) {
            drawcalls[i] = new Drawcall();
        }
    }
}

class Drawcall {
    float[] coordinates = new float[4];
    short[] color;
}
